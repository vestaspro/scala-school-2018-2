package wtf.scala.e03


object  Fibonacci {

  /**
    * Compute the n th Fibonacci number using recursion
    * https://en.wikipedia.org/wiki/Fibonacci_number
    *
    * We assume that fib(1) = 0, fib(2) = 1, and so on
    * @param n
    * @return
    */

  def fibRecursive(n: Int): Long = {
     import scala.annotation.tailrec
     @tailrec
     def fib(x: Int, prev: Long = 0, next: Long =1):Long = x match {
       case 1 => prev
       case 2 => next
       case _ => fib(x -1, next, (next+prev))
     }
     fib(n)
  }

  /**
    * Compute the n th Fibonacci number using memoization
    * https://en.wikipedia.org/wiki/Fibonacci_number
    *
    * We assume that fib(1) = 0, fib(2) = 1, and so on
    *
    * Hint: to store computed Fibonacci numbers you can use scala.collection.mutable.HashMap[Int, Long]
    * @param n
    * @return
    */
  import scala.collection.mutable.HashMap
  def fibMemo(n: Int): Long = {
     val fib: scala.collection.mutable.HashMap[Int, Long] = HashMap(1 -> 0L, 2 -> 1L)
     (3 to n).foreach {  i => fib += i -> (fib(i-1) + fib(i -2) ).toLong }
     fib(n)
  }

}
